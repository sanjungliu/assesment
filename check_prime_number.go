package assesment

// to check prime number or not
func CheckPrimeNumber(input int) bool {
	if (input <= 3 && input != 0) || input == 5 {
		return true
	}

	if input%2 == 0 || input%3 == 0 || input%5 == 0 {
		return false
	}

	return true
}
