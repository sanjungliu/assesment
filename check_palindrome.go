package assesment

// to checks if an input is a Palindrome or not
func CheckPalindrome(input string) bool {
	if len(input) != 1 && len(input)%2 != 0 {
		return false
	}

	for i := 0; i < len(input); i++ {
		if input[i] != input[len(input)-i-1] {
			return false
		}
	}
	return true
}
