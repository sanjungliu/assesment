package assesment

import (
	"strconv"
)

// sort given input numerically
func SortNumbers(input int) int {
	str := strconv.Itoa(input)
	sliceOfInt := []int{}

	for i := 0; i < len(str); i++ {
		i, _ := strconv.Atoi(string(str[i]))
		sliceOfInt = append(sliceOfInt, i)
	}

	for j := 1; j < len(sliceOfInt); j++ {
		for i := 1; i < len(sliceOfInt); i++ {
			if sliceOfInt[i-1] > sliceOfInt[i] {

				temp := sliceOfInt[i-1]
				sliceOfInt[i-1] = sliceOfInt[i]
				sliceOfInt[i] = temp
			}
		}
	}

	str = ""
	for i := 0; i < len(sliceOfInt); i++ {
		str += strconv.Itoa(sliceOfInt[i])
	}

	input, _ = strconv.Atoi(str)

	return input
}
