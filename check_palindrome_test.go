package assesment_test

import (
	"testing"

	"gitlab.com/sanjungliu/assesment"
)

func Test_CheckPalindrome(t *testing.T) {
	cases := map[string]struct {
		input       string
		expectation bool
	}{
		"oneIndexInput": {
			input:       "a",
			expectation: true,
		},
		"sameTwoIndexInput": {
			input:       "aa",
			expectation: true,
		},
		"notModulusByTwo": {
			input:       "aaa",
			expectation: false,
		},
		"palindromeLongInput": {
			input:       "kodokkodok",
			expectation: true,
		},
		"palindromeShortInput": {
			input:       "oppo",
			expectation: true,
		},
		"notPalindromeLongInput": {
			input:       "inputyangpanjangsekali",
			expectation: false,
		},
		"notPalindromeShortInput": {
			input:       "upps",
			expectation: false,
		},
	}

	for name, tc := range cases {
		t.Run(name, func(t *testing.T) {
			actual := assesment.CheckPalindrome(tc.input)

			if actual != tc.expectation {
				t.Errorf("Expected %v but got %v", tc.expectation, actual)
			}
		})
	}
}
