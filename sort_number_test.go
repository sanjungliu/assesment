package assesment_test

import (
	"testing"

	"gitlab.com/sanjungliu/assesment"
)

func Test_SortNumbers(t *testing.T) {
	cases := map[string]struct {
		input       int
		expectation int
	}{
		"Long": {
			input:       52597231834673233,
			expectation: 12223333345567789,
		},
		"Short": {
			input:       8396,
			expectation: 3689,
		},
	}

	for name, tc := range cases {
		t.Run(name, func(t *testing.T) {
			actual := assesment.SortNumbers(tc.input)

			if actual != tc.expectation {
				t.Errorf("Expected %v but got %v", tc.expectation, actual)
			}
		})
	}
}
