-- Most frequent "credit_card_type"

    SELECT
        "user"."credit_card_type",
        count(*) AS total
    FROM
        "user"
    GROUP BY
        "user"."credit_card_type"
    HAVING
        count(*) = (
	SELECT
        max(total)
	FROM
		(
		SELECT
			"user"."credit_card_type",
			count(*) AS total
		FROM
			"user"
		GROUP BY
			"user"."credit_card_type") "user"
        );