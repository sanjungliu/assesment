package assesment_test

import (
	"testing"

	"gitlab.com/sanjungliu/assesment"
)

func Test_CheckPrimeNumber(t *testing.T) {
	cases := map[string]struct {
		input       int
		expectation bool
	}{
		"1": {
			input:       1,
			expectation: true,
		},
		"2": {
			input:       2,
			expectation: true,
		},
		"3": {
			input:       3,
			expectation: true,
		},
		"4": {
			input:       4,
			expectation: false,
		},
		"5": {
			input:       5,
			expectation: true,
		},
		"9": {
			input:       9,
			expectation: false,
		},
		"11": {
			input:       11,
			expectation: true,
		},
		"73": {
			input:       73,
			expectation: true,
		},
		"99": {
			input:       99,
			expectation: false,
		},
	}

	for name, tc := range cases {
		t.Run(name, func(t *testing.T) {
			actual := assesment.CheckPrimeNumber(tc.input)

			if actual != tc.expectation {
				t.Errorf("Expected %v but got %v", tc.expectation, actual)
			}
		})
	}

}
